import { createRouter, createWebHashHistory } from 'vue-router'
import PokemonView from '../modules/pokemon/views/PokemonPage'
import DescriptionView from '../views/DescriptionView.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: PokemonView
  },

  {
    path: '/pokemon',
    name: 'pokemon',
    component: DescriptionView
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
