import { createStore } from 'vuex'

import pokemons from "../modules/pokemon/store";

const store = createStore({
  modules: {
    pokemons
  },
});

export default store;
