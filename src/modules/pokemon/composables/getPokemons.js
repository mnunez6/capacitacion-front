/* import pokemonApi from "../api/pokemonApi"; 
*/

import { computed } from "vue"; 
import { useStore } from "vuex";

const usePokemons = () => {
  const store = useStore();


  const getPokemons = async () => {
    const resp = await store.dispatch("pokemons/lapras");
    return resp;
  };

  return{
    getPokemons,

    pokemonsState: computed(
      () => store.getters["/lapras"]
    ),
  };
};


export default usePokemons;
