
import api from "@/api/pokemonApi"

export const getPokemons = async({ commit }) => {
    try {
        const { data: info } = await api.post(`/pokemon/getPokemons/lapras`);
        const { status, data, message } = info;

        if (status != "success") return { ok: false, message };

        commit("setPokemons", data);
        return { ok: true, message, data };
    } catch (err) {
        let msg;
        if (err.response) {
            msg = err.response.data.message;
        } else if (err.request) {
            msg = err.request;
        } else {
            msg = err.message;
        }
        return { ok: false, message: msg };
    }
};


