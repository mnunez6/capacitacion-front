import axios from 'axios'

const pokemonApi = axios.create({
    baseURL: 'https://pokeapi.co/api/v2/pokemon'
    //https://pokeapi.co/api/v2/pokemon?limit=151&offset=0
})

export default pokemonApi